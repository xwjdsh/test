package main

import (
	"flag"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	session *mgo.Session
	dsn     = flag.String("dsn", "", "data source name")
	addr    = flag.String("a", ":1323", "address")
)

type Record struct {
	Name  string `json:"name"`
	Favor string `json:"favor"`
}

func main() {
	flag.Parse()

	initMongoSession()
	defer session.Clone()

	initEcho()
}

func initMongoSession() {
	var err error
	session, err = mgo.Dial(*dsn)
	if err != nil {
		panic(err)
	}
}

func initEcho() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/users/", users)

	// Start server
	e.Logger.Fatal(e.Start(*addr))
}

func users(c echo.Context) error {
	name := c.QueryParam("name")
	favor := c.QueryParam("favor")
	m := bson.M{}
	if name != "" {
		m["name"] = name
	}

	if favor != "" {
		m["favor"] = favor
	}

	collection := session.Copy().DB("test-db").C("test")
	var rs []*Record
	if err := collection.Find(m).All(&rs); err != nil {
		code := http.StatusInternalServerError
		return c.String(code, http.StatusText(code))
	}
	if len(rs) == 0 {
		code := http.StatusNotFound
		return c.String(code, http.StatusText(code))
	}

	return c.JSON(http.StatusOK, rs)
}
