module github.com/xwjdsh/test

go 1.12

require (
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.0.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
